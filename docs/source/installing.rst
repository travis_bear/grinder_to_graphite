Installation Notes
******************

Install via Pip
===============
g2g is written in Python.  The best way to install g2g is via pip.
::

    pip install grinder_to_graphite 

pyyaml binaries
---------------
g2g depends on the pyyaml package.  pyyaml tries to compile
a (fast) binary yaml parser for you, using the python development
libraries.  On most systems, the Python development libraries
are not installed by default, in which case the pyyaml installer
falls back to using a pure Python parser implementation.

If you want to have the python development libraries in place 
to get the binary yaml parser, you can install them easily.

On debian-based systems, they can be installed
like this.
::

    sudo apt-get apt-get install python-dev

On Red-Hat based systems, you can get it like this.
::

    yum install python-devel


Virtualenv
==========
Using virtualenv is not required.  You can easily install g2g into
your core Python environment.  However, you will likely need to be
an admin user to to this, or you will need to preface the pip
command with *sudo*.  In general, this is not the recommended
approach.

Going with virtualenv, and installing
g2g into its own environment is the recommended way to proceed.


Pypi
====
g2g runs more than twice as fast on pypy as it does on standard cpython.
If you will regularly be ingesting large amounts of data, consider
installing pypy.

http://pypy.org/download.html


Generate Configuration File
=========================== 
Once pip has installed g2g, you will need to generate a
g2g config file, and edit it to be suitable for your own
environment.  g2g comes with a command-line option to
generate a sample config file
::

    g2g -e


This will generate a file named 'g2g.sample.yaml'
in the current directory which you can use as the basis for 
creating your own configuration.


Graphite
========
There must be a running installation of Graphite on your
network for g2g to forward data to.  This g2g documentation
does not cover the 
details on setting up and configuring Graphite.  For more
information on this topic, see the Graphite web site.

http://graphite.wikidot.com/

