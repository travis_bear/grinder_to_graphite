Compatibility
*************

g2g has been tested on these versions of Python:

 * python 2.7
 * pypy
 
g2g is not compatible with Python versions earlier than 2.7.

Python 3 support is a priority, and will be added soon.
